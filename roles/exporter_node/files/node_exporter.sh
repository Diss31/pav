#!/sbin/openrc-run

#Node Exporter Service for Alpine OS

name="Prometheus Node Exporter"
command="/etc/node_exporter/node_exporter"
command_args=""

#Launch the command in background and create a pid file
command_background=true
pidfile="/run/node_exporter.pid"

# Set command's user and group
command_user="prometheus:prometheus"
