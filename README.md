# Prometheus-Ansible-Vagrant (PAV) project

Personnal project to explore Ansible features. The goal is to install a Prometheus monitoring on several nodes, provided by Vagrant.

The default architecture is:

* 1 master node, with Prometheus installed at node launch.

* 3 target nodes, with several distributions (Ubuntu Xenial, Alpine 3.8.5, Debian Buster)

## Current monitoring features

1. Prometheus auto-monitoring 
2. Node exporters on targets
3. cAdvisor on targets

## Launch commands

```
vagrant up
ansible-playbook targets.yml -i inventory
```

## How to add new target node 

Warning: the support is guaranted only on above distributions

1. Edit Vagrantfile
2. Edit Ansible inventory
3. Edit Prometheus target (./roles/prometheus_node/files/prometheus.yml)

## Possible improvements

* Implement CAdvisor test role
* Add other distribution and OS support
* Generate ssh connection variables and Prometheus targets from Vagrantfile (no more hard-coded ports in inventory) 
* Allow multiple Prometheus nodes
* Automatically get last version of Prometheus and Node Exporter