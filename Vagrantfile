# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

#Import variables
require 'yaml'
current_dir = File.dirname(File.expand_path(__FILE__))
vars = YAML.load_file("#{current_dir}/vagrant_config.yml")

Vagrant.configure("2") do |config|

  #Disabled synced folder
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Master node, hosting Prometheus
  config.vm.define "master" do |master|
    master.vm.box = "ubuntu/xenial64"
    master.vm.hostname = 'master'

    # IP for communication between nodes
    master.vm.network :private_network, ip: vars['master_ip']

    # Forward port from the local ssh port
    master.vm.network :forwarded_port, guest: 22, host: vars['master_port'], id: "ssh"

    master.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "master"]
    end

    # Run dedicated playbook after VM launch
    master.vm.provision "ansible" do |ansible|
      ansible.playbook = "master.yml"
      ansible.inventory_path = "./inventory"
    end
  end

  # Ubuntu 1 node
  config.vm.define "ubuntu" do |ubuntu|
    ubuntu.vm.box = "ubuntu/xenial64"
    ubuntu.vm.hostname = 'ubuntu'

    ubuntu.vm.network :private_network, ip: vars['ubuntu_ip']
    ubuntu.vm.network :forwarded_port, guest: 22, host: vars['ubuntu_port'], id: "ssh"

    ubuntu.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "ubuntu"]
    end
  end

  # Debian 1 node
  config.vm.define "debian" do |debian|
    debian.vm.box = "debian/buster64"

    debian.vm.network :private_network, ip: vars['debian_ip']
    debian.vm.network :forwarded_port, guest: 22, host: vars['debian_port'], id: "ssh"

    debian.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "debian"]
    end
  end

  # Alpine 1 node
  config.vm.define "alpine" do |alpine|
    alpine.vm.box = "generic/alpine38"

    alpine.vm.network :private_network, ip: vars['alpine_ip']
    alpine.vm.network :forwarded_port, guest: 22, host: vars['alpine_port'], id: "ssh"

    # Install python3 (Ansible requirement) when VM is up
    alpine.vm.provision "shell",
      inline: "sudo apk add python3"

    alpine.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "alpine"]
    end
  end
end